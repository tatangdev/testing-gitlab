Config user

```sh
git config --global user.name "FIRST_NAME"
git config --global user.email "MY_NAME@example.com"
```

Menginisiasikan git project

```sh
git init
```

Memindahkan perubahan kedalam staging area

```sh
git add .
```

Menambahakan berita acara, dan memindahkan kedalam local repositody

```sh
git commit -m "commit message"
```
